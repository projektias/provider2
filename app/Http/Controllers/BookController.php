<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;

use App\Http\Requests;

class BookController extends Controller
{
    //
    public function index(){
        return response()->json(Book::limit(10)->get());
    }
}
